#ifndef FTFS_
#define FTFS_

#include <sstream>
#include <string>
#include <cmath>
#include <iomanip>

// Usage 
// std::string mystring = (23.21389, 5);
std::string ftfs(float f, unsigned int max_chars);

#endif  // FTFS_
