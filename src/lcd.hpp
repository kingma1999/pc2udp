#ifndef LCD_HPP_
#define LCD_HPP_

#include <wiringPiI2C.h>
#include <wiringPi.h>

#define I2C_ADDR   0x27 // I2C device address
#define LINE1  0x80 // 1st line
#define LINE2  0xC0 // 2nd line
#define LINE3  0x94 // 3th line
#define LINE4  0xD4 // 4th line

class LCD {
    private:
        void lcd_byte(int bits, int mode);
        void lcd_toggle_enable(int bits);
    public:
        void Home(void);
        void printI(int i);
        void printF(float myFloat);
        void setCursor(int line); //move cursor
        void Clear(void); // clr LCD return home
        void printS(const char *s);
        void printC(char val);
        void SetCustomChars(void);

        int fd;

        LCD();
};

#endif  // LCD_HPP_
