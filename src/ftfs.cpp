#include "ftfs.hpp"
#include <iomanip>

std::string ftfs(float f, unsigned int max_chars)
{
    std::stringstream return_sstream("");
    return_sstream << std::setprecision(max_chars) << f;
    return return_sstream.str();
}
