#define SMS_UDP_PORT 5606

#include "UDPReader.hpp"

int main(int argc, char* argv[])
{
    UDPReader pc2reader(SMS_UDP_PORT);
    pc2reader.Listen();
}
