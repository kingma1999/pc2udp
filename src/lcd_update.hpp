#ifndef LCD_UPDATE_HPP_
#define LCD_UPDATE_HPP_

#include "SMS_UDP_Definitions.hpp"
#include "processing.hpp"
#include "lcd.hpp"
#include "ftfs.hpp"
#include <wiringPi.h>

void UpdateFuel(LCD lcd, float FuelAverage, float PitFuelNeeded, float LapsFuelLeft, int RaceBase);
void UpdateOpponentsRace(LCD lcd);
void UpdateCircuit(LCD lcd);
void UpdateCar(LCD lcd, float RideHeightNormal, float RideHeightDif);
void UpdateOpponentsQuali(LCD lcd);

#endif // LCD_UPDATE_HPP_