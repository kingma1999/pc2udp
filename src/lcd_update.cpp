#include "SMS_UDP_Definitions.hpp"
#include "processing.hpp"
#include "lcd.hpp"
#include "ftfs.hpp"
#include "lcd_update.hpp"
#include <wiringPi.h>

// ----------- Globals defined elsewhere ----------- 
extern float FuelStart;
extern float FuelEnd;
extern float FuelUsedPerLap[5];
extern unsigned int OldMainDriverLapsComplete;
extern int FirstRun;
extern bool PressCheck;

extern float LapDifBehind[3];
extern float LapDifFront[3];
extern float LapDifFirst[3];

extern float QualiDifBehind[3];
extern float QualiDifFront[3];
extern float QualiDifFirst[3];

extern float AmbTemp;
extern float TrkTemp;
extern float RainDen;

extern float FuelAverage;
extern float PitFuelNeeded;
extern float LapsFuelLeft;
// ------------------------------------------------- 

void UpdateFuel(LCD lcd, float FuelAverage, float PitFuelNeeded, float LapsFuelLeft, int RaceBase) {
    lcd.setCursor(LINE1);
    lcd.printS("F/Lap: ");
    lcd.printS(ftfs(FuelAverage, 4).c_str());
    lcd.printS(" l");
    lcd.setCursor(LINE2);
    lcd.printS("PitFReq: ");
    lcd.printS(ftfs(PitFuelNeeded, 4).c_str());
    lcd.printS(" l");
    lcd.setCursor(LINE3);
    lcd.printS("L left: ");
    lcd.printS(ftfs(LapsFuelLeft, 4).c_str());
    lcd.printS(" Laps");
    lcd.Home();
}

void UpdateOpponentsRace(LCD lcd) {
    int AlignFirst = 3;
    int AlignFront = 9;
    int AlignBack = 15;
    lcd.setCursor(LINE1);
    lcd.printS("Dif/L 1");
    lcd.setCursor(LINE1 + AlignFront);
    lcd.printS("Front");
    lcd.setCursor(LINE1 + AlignBack);
    lcd.printS("Back");
    lcd.setCursor(LINE2 + AlignFirst);
    lcd.printS(ftfs(LapDifFirst[0], 4).c_str());
    lcd.setCursor(LINE3 + AlignFirst);
    lcd.printS(ftfs(LapDifFirst[1], 4).c_str());
    lcd.setCursor(LINE4 + AlignFirst);
    lcd.printS(ftfs(LapDifFirst[2], 4).c_str());
    lcd.setCursor(LINE2 + AlignFront);
    lcd.printS(ftfs(LapDifFront[0], 4).c_str());
    lcd.setCursor(LINE3 + AlignFront);
    lcd.printS(ftfs(LapDifFront[1], 4).c_str());
    lcd.setCursor(LINE4 + AlignFront);
    lcd.printS(ftfs(LapDifFront[2], 4).c_str());
    lcd.setCursor(LINE2 + AlignBack);
    lcd.printS(ftfs(LapDifBehind[0], 4).c_str());
    lcd.setCursor(LINE3 + AlignBack);
    lcd.printS(ftfs(LapDifBehind[1], 4).c_str());
    lcd.setCursor(LINE4 + AlignBack);
    lcd.printS(ftfs(LapDifBehind[2], 4).c_str());
    lcd.Home();
}

void UpdateCircuit(LCD lcd) {
    lcd.setCursor(LINE1);
    lcd.printS("Ambient: ");
    lcd.printS(ftfs(AmbTemp, 4).c_str());
    lcd.printS(" C");
    lcd.setCursor(LINE2);
    lcd.printS("Track:   ");
    lcd.printS(ftfs(TrkTemp, 4).c_str());
    lcd.printS(" C");
    lcd.setCursor(LINE3);
    lcd.printS("Rain:    ");
    lcd.printS(ftfs(RainDen, 4).c_str());
    lcd.printS(" ");
    lcd.Home();
}

void UpdateCar(LCD lcd, float RideHeightNormal, float RideHeightDif) {
    lcd.Home();
}

void UpdateOpponentsQuali(LCD lcd) {
    int AlignFirst = 3;
    int AlignFront = 9;
    int AlignBack = 15;
    lcd.setCursor(LINE1);
    lcd.printS("SecDf 1");
    lcd.setCursor(LINE1 + AlignFront);
    lcd.printS("Front");
    lcd.setCursor(LINE1 + AlignBack);
    lcd.printS("Back");
    lcd.setCursor(LINE2 + AlignFirst);
    lcd.printS(ftfs(QualiDifFirst[0], 4).c_str());
    lcd.setCursor(LINE3 + AlignFirst);
    lcd.printS(ftfs(QualiDifFirst[1], 4).c_str());
    lcd.setCursor(LINE4 + AlignFirst);
    lcd.printS(ftfs(QualiDifFirst[2], 4).c_str());
    lcd.setCursor(LINE2 + AlignFront);
    lcd.printS(ftfs(QualiDifFront[0], 4).c_str());
    lcd.setCursor(LINE3 + AlignFront);
    lcd.printS(ftfs(QualiDifFront[1], 4).c_str());
    lcd.setCursor(LINE4 + AlignFront);
    lcd.printS(ftfs(QualiDifFront[2], 4).c_str());
    lcd.setCursor(LINE2 + AlignBack);
    lcd.printS(ftfs(QualiDifBehind[0], 4).c_str());
    lcd.setCursor(LINE3 + AlignBack);
    lcd.printS(ftfs(QualiDifBehind[1], 4).c_str());
    lcd.setCursor(LINE4 + AlignBack);
    lcd.printS(ftfs(QualiDifBehind[2], 4).c_str());
    lcd.Home();
}
