
/*
 *
 * by Lewis Loflin www.bristolwatch.com lewis@bvu.net
 * http://www.bristolwatch.com/rpi/i2clcd.htm
 * Using wiringPi by Gordon Henderson
 *
 *
 * Port over lcd_i2c.py to C and added improvements.
 * Supports 16x2 and 20x4 screens.
 * This was to learn now the I2C lcd displays operate.
 * There is no warrenty of any kind use at your own risk.
 *
 */

#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>
#include "lcd.hpp"
#include <stdint.h>
#include <unistd.h>

// Define some device parameters

// Define some device constants
#define LCD_CHR  1 // Mode - Sending data
#define LCD_CMD  0 // Mode - Sending command

#define LCD_BACKLIGHT   0x08  // On
// LCD_BACKLIGHT = 0x00  # Off

#define ENABLE  0b00000100 // Enable bit

uint8_t CustomChars[8][8] = {
    {   //0
        0b00000,
        0b00100,
        0b01110,
        0b10101,
        0b00100,
        0b00100,
        0b00000,
        0b00000
    },
    {   //1
        0b00000,
        0b00111,
        0b00011,
        0b00101,
        0b01000,
        0b10000,
        0b00000,
        0b00000
    },
    {   //2
        0b00000,
        0b10000,
        0b01000,
        0b00101,
        0b00011,
        0b00111,
        0b00000,
        0b00000
    },
    {   //3
        0b00000,
        0b00100,
        0b00100,
        0b10101,
        0b01110,
        0b00100,
        0b00000,
        0b00000
    },
    {   //4
        0b00000,
        0b00001,
        0b00010,
        0b10100,
        0b11000,
        0b11100,
        0b00000,
        0b00000
    },
    {   //5
        0b00000,
        0b11100,
        0b11000,
        0b10100,
        0b00010,
        0b00001,
        0b00000,
        0b00000
    },
    {   //6
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b00000
    },
    {   //7
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b00000,
        0b00000
    }
};

// float to string
void LCD::printF(float myFloat)   {
    char buffer[20];
    sprintf(buffer, "%4.2f",  myFloat);
    printS(buffer);
}

// int to string
void LCD::printI(int i)   {
    char array1[20];
    sprintf(array1, "%d",  i);
    printS(array1);
}

// clr lcd go home loc 0x80
void LCD::Clear(void)   {
    lcd_byte(0x01, LCD_CMD);
    lcd_byte(0x02, LCD_CMD);
}

void LCD::Home(void)   {
    lcd_byte(0x02, LCD_CMD);
}

// go to location on LCD
void LCD::setCursor(int line)   {
    lcd_byte(line, LCD_CMD);
}

// out char to LCD at current position
void LCD::printC(char val)   {

    lcd_byte(val, LCD_CHR);
}

// this allows use of any size string
void LCD::printS(const char *s)   {

    while ( *s ) lcd_byte(*(s++), LCD_CHR);

}

void LCD::lcd_byte(int bits, int mode)   {

    //Send byte to data pins
    // bits = the data
    // mode = 1 for data, 0 for command
    int bits_high;
    int bits_low;
    // uses the two half byte writes to LCD
    bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT ;
    bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT ;

    // High bits
    wiringPiI2CReadReg8(fd, bits_high);
    lcd_toggle_enable(bits_high);

    // Low bits
    wiringPiI2CReadReg8(fd, bits_low);
    lcd_toggle_enable(bits_low);
}

void LCD::lcd_toggle_enable(int bits)   {
    // Toggle enable pin on LCD display
    delayMicroseconds(500);
    wiringPiI2CReadReg8(fd, (bits | ENABLE));
    delayMicroseconds(500);
    wiringPiI2CReadReg8(fd, (bits & ~ENABLE));
    delayMicroseconds(500);
}

void LCD::SetCustomChars(void) {
    for (uint8_t i = 0; i < 8; i++) {
        uint8_t Location = i & 0x07;
        lcd_byte(0x40 | (Location << 3), LCD_CMD);
        usleep(50);
        for (uint8_t j = 0; j < 8; j++) {
            lcd_byte(CustomChars[i][j], LCD_CHR);
            usleep(50);
        }
    }
}

LCD::LCD()   {
    fd = wiringPiI2CSetup(I2C_ADDR);

    // Initialise display
    lcd_byte(0x33, LCD_CMD); // Initialise
    lcd_byte(0x32, LCD_CMD); // Initialise
    lcd_byte(0x06, LCD_CMD); // Cursor move direction
    lcd_byte(0x0C, LCD_CMD); // 0x0F On, Blink Off
    lcd_byte(0x28, LCD_CMD); // Data length, number of lines, font size
    lcd_byte(0x01, LCD_CMD); // Clear display
    delayMicroseconds(500);
}
