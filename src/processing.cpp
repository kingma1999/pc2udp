#include "SMS_UDP_Definitions.hpp"
#include "processing.hpp"
#include "lcd.hpp"
#include "ftfs.hpp"
#include "lcd_update.hpp"
#include <wiringPi.h>
#include <chrono>

#include <iostream>
#include <math.h>

#define M_PI 3.14159265358979323846
float FuelStart = 0;
float FuelEnd = 0;
float FuelUsedPerLap[5];
unsigned int OldMainDriverLapsComplete = -1;
int FirstRun = 0;
bool PressCheck = FALSE;

float LapDifBehind[3];
float LapDifFront[3];
float LapDifFirst[3];

float QualiDifBehind[3];
float QualiDifFront[3];
float QualiDifFirst[3];

float AmbTemp;
float TrkTemp;
float RainDen;

float FuelAverage;
float PitFuelNeeded;
float LapsFuelLeft;

float RideHeightNormal;
float RideHeightDif;

/*  
    0 = off
    1 = FuelInfo
    2 = OpponentsInfoRace
    3 = CircuitInfo
    4 = CarInfo
    5 = OpponentsInfoQuali
*/

int ScreenState = 1;
int OldScreenState = 0;

int RaceBase;

struct driver
{
    int index;
    int racePosition;
    float lapTimes[3];
    float QualiTime;
    float QualiSecTimes[3];
};

driver drivers[16];

void myInterrupt() {
    static std::chrono::time_point<std::chrono::system_clock> previousCall;
    auto current_time = std::chrono::system_clock::now();
    if (current_time-previousCall < std::chrono::milliseconds(600)) return;

    previousCall = current_time;
    ScreenState = ScreenState +1;
    if (ScreenState > 5) {
           ScreenState = 0;
    }

    piLock(0);
    if (OldScreenState != ScreenState) {
        DataProcessor::lcd.Clear();
        switch (ScreenState)
        {
        case 0:
            break;
        case 1:
            UpdateFuel(DataProcessor::lcd, FuelAverage, PitFuelNeeded, LapsFuelLeft, RaceBase);
            break;
        case 2:
            UpdateOpponentsRace(DataProcessor::lcd);
            break;
        case 3:
            UpdateCircuit(DataProcessor::lcd);
            break;
        case 4:
            UpdateCar(DataProcessor::lcd, RideHeightNormal, RideHeightDif);
            break;
        case 5:
            UpdateOpponentsQuali(DataProcessor::lcd);
            break;
        }
        OldScreenState = ScreenState;
    }
    piUnlock(0);
    std::cout << "ScreenState: " << ScreenState << std::endl;
}

void RestartInterrupt() {
    FuelStart = 0;
    FuelEnd = 0;
    float FuelUsedPerLap[5];
    OldMainDriverLapsComplete = -1;
    FirstRun = 0;
    PressCheck = FALSE;

    for (int i = 0; i < 3; i++) {
        LapDifBehind[i] = 0;
        LapDifFront[i] = 0;
        LapDifFirst[i] = 0;

        QualiDifBehind[i] = 0;
        QualiDifFront[i] = 0;
        QualiDifFirst[i] = 0;
    }

    AmbTemp = 0;
    TrkTemp = 0;
    RainDen = 0;

    FuelAverage = 0;
    PitFuelNeeded = 0;
    LapsFuelLeft = 0;

    RideHeightNormal = 0;
    RideHeightDif = 0;

    ScreenState = 1;
    OldScreenState = 0;

    for (int i = 0; i < 16; i++) {
        drivers[i].index = 0;
        drivers[i].racePosition = 0;
        drivers[i].QualiTime = 0;
        for (int j = 0; i < 3; i++) {
            drivers[i].lapTimes[j] = 0;
            drivers[i].QualiSecTimes[j] = 0;
        }
    }
}

void DataProcessor::OnReceiveNewPacket()
{
    RaceBase = dataRace.sLapsTimeInEvent>>(8*sizeof(dataRace.sLapsTimeInEvent)-1);
    int SessionState = dataGameState.mGameState>>(8*sizeof(dataGameState.mGameState)-6);
    SessionState = SessionState&0x07;

    if (FirstRun == 0 && (int)((dataGameState.mGameState&0x1c)>>2) == 4) {
        FirstRun = 1;
        FuelEnd = dataTelemetry.sFuelLevel;
    }

    if (FirstRun == 1 && SessionState == 0) {
        FirstRun = 0;
        RestartInterrupt();
    }

    int MainDriverIndex = (int)dataTelemetry.sViewedParticipantIndex;
    unsigned int MainDriverLapsComplete = dataTimings.sPartcipants[MainDriverIndex].sCurrentLap - 1;

    if (MainDriverLapsComplete > OldMainDriverLapsComplete) {
        FuelStart = FuelEnd;
        FuelEnd = dataTelemetry.sFuelLevel;
        for (int i = 4; i >= 0; i--) {
            FuelUsedPerLap[i+1] = FuelUsedPerLap[i];
        }

        if (RaceBase == 0) {
            FuelUsedPerLap[0] = (FuelStart - FuelEnd)*dataTelemetry.sFuelCapacity;
        }

        else if (RaceBase == 1) {
            FuelUsedPerLap[0] = (FuelStart - FuelEnd)*dataTelemetry.sFuelCapacity / dataTimeStats.sStats.sParticipants[MainDriverIndex].sLastLapTime;
        }

        float FuelSum = 0;
        for (int i = 0; i < 5; i++) {
            FuelSum = FuelSum + FuelUsedPerLap[i];
        }
        int DifNumb;
        if (MainDriverLapsComplete < 5) {
            DifNumb = MainDriverLapsComplete;
            std::cout << "DifNumb " << DifNumb << std::endl;
        }
        else if (MainDriverLapsComplete > 5) {
            DifNumb = 5;
        }
        FuelAverage = FuelSum / DifNumb;
        int LapsInRace = -(int)dataTimings.sEventTimeRemaining + MainDriverLapsComplete;
        std::cout << "Event time remaining: " << -(int)dataTimings.sEventTimeRemaining << std::endl;
        
        std::cout << "Laps in event: " << LapsInRace << std::endl;

        if (RaceBase == 0) {
            LapsFuelLeft = ((dataTelemetry.sFuelLevel * (float)dataTelemetry.sFuelCapacity) / FuelAverage) - (LapsInRace - MainDriverLapsComplete);
            PitFuelNeeded = ((LapsInRace - MainDriverLapsComplete) * FuelAverage) - (dataTelemetry.sFuelLevel * (float)dataTelemetry.sFuelCapacity);
        }
        
        if (RaceBase == 1) {
            LapsFuelLeft = ((dataTelemetry.sFuelLevel * (float)dataTelemetry.sFuelCapacity) / FuelAverage);
            PitFuelNeeded = ((LapsInRace - MainDriverLapsComplete) * FuelAverage) - (dataTelemetry.sFuelLevel * (float)dataTelemetry.sFuelCapacity);
        }
        if (ScreenState == 1) {
            piLock(0);
            UpdateFuel(lcd, FuelAverage, PitFuelNeeded, LapsFuelLeft, RaceBase);
            piUnlock(0);
        }
    }

    if (OldMainDriverLapsComplete > MainDriverLapsComplete) {
        OldMainDriverLapsComplete = 0;
    }
    else {
        OldMainDriverLapsComplete = MainDriverLapsComplete;
    }

    if (lastPacketType == eTimings) {
        for (int i = 0; i < 16; i++) {
            drivers[i].index = i;
            drivers[i].racePosition = dataTimings.sPartcipants[i].sRacePosition;
            
            if (dataTimeStats.sStats.sParticipants[i].sLastLapTime != drivers[i].lapTimes[0]) {
                drivers[i].lapTimes[2] = drivers[i].lapTimes[1];
                drivers[i].lapTimes[1] = drivers[i].lapTimes[0];
                drivers[i].lapTimes[0] = dataTimeStats.sStats.sParticipants[i].sLastLapTime;
                std::cout << "LapTimeChange: " << drivers[i].lapTimes[1] << std::endl;

                if (drivers[i].racePosition == dataTimings.sPartcipants[MainDriverIndex].sRacePosition + 1) {
                    for (int j = 0; j < 3; j++) {
                        LapDifBehind[i] = drivers[MainDriverIndex].lapTimes[j]-drivers[i].lapTimes[j];
                    }
                }
                else if (drivers[i].racePosition == dataTimings.sPartcipants[MainDriverIndex].sRacePosition - 1) {
                    for (int j = 0; j < 3; j++) {
                        LapDifFront[i] = drivers[MainDriverIndex].lapTimes[j]-drivers[i].lapTimes[j];
                        std::cout << "FrontDif: " << LapDifFront[i] << std::endl;
                    }
                }
                else if (drivers[i].racePosition == 1) {
                    for (int j = 0; j < 3; j++) {
                        LapDifFirst[i] = drivers[MainDriverIndex].lapTimes[j]-drivers[i].lapTimes[j];
                    }
                } 
            }
        }

        if (ScreenState == 2) {
            piLock(0);
            UpdateOpponentsRace(lcd);
            piUnlock(0);
        }

        for (int i = 0; i < 16; i++) {
            drivers[i].index = i;
            drivers[i].racePosition = dataTimings.sPartcipants[i].sRacePosition;
            
            if (dataTimeStats.sStats.sParticipants[i].sFastestLapTime != drivers[i].QualiTime) {
                drivers[i].QualiTime = dataTimeStats.sStats.sParticipants[i].sFastestLapTime;
                drivers[i].QualiSecTimes[0] = dataTimeStats.sStats.sParticipants[i].sFastestSector1Time;
                drivers[i].QualiSecTimes[1] = dataTimeStats.sStats.sParticipants[i].sFastestSector2Time;
                drivers[i].QualiSecTimes[2] = dataTimeStats.sStats.sParticipants[i].sFastestSector3Time;
            }

            if (drivers[i].racePosition == dataTimings.sPartcipants[MainDriverIndex].sRacePosition + 1) {
                for (int j = 0; j < 3; j++) {
                    QualiDifBehind[i] = drivers[i].QualiSecTimes[j]-drivers[MainDriverIndex].QualiSecTimes[j];
                }
            }
            else if (drivers[i].racePosition == dataTimings.sPartcipants[MainDriverIndex].sRacePosition - 1) {
                for (int j = 0; j < 3; j++) {
                    QualiDifFront[i] = drivers[i].QualiSecTimes[j]-drivers[MainDriverIndex].QualiSecTimes[j];
                }
            }
            else if (drivers[i].racePosition == 1) {
                for (int j = 0; j < 3; j++) {
                    QualiDifFirst[i] = drivers[i].QualiSecTimes[j]-drivers[MainDriverIndex].QualiSecTimes[j];
                }
            }
        }

        if (ScreenState == 5) {
            piLock(0);
            UpdateOpponentsQuali(lcd);
            piUnlock(0);
        }

        if (dataGameState.sAmbientTemperature != AmbTemp) {
            AmbTemp = (float)dataGameState.sAmbientTemperature;
        }

        if ((float)dataGameState.sTrackTemperature != TrkTemp) {
            TrkTemp = (float)dataGameState.sTrackTemperature;
        }

        if ((float)dataGameState.sRainDensity != RainDen*100) {
            RainDen = (float)dataGameState.sRainDensity*100;
        }

        float CarAngle = dataTelemetry.sOrientation[1]+M_PI;
        float WindAngle = (atan2((float)dataGameState.sWindDirectionY, (float)dataGameState.sWindDirectionX))+((1/2)*M_PI);
        float AngleDif = WindAngle - CarAngle;
        //std::cout << "dif angle " << AngleDif << std::endl;

        //UnixArrows!!!!!!

        if (ScreenState == 3) {
            piLock(0);
            UpdateCircuit(lcd);
            piUnlock(0);
        }
    }

    if (lastPacketType == eCarPhysics) {
        float RideHeightFront = (dataTelemetry.sRideHeight[0]+dataTelemetry.sRideHeight[1])/2;
        float RideHeightRear = (dataTelemetry.sRideHeight[2]+dataTelemetry.sRideHeight[3])/2;
        if (RideHeightFront < RideHeightRear) {
            RideHeightNormal = RideHeightFront;
        }
        else if (RideHeightRear < RideHeightFront) {
            RideHeightNormal = RideHeightRear;
        }
        RideHeightDif = RideHeightRear - RideHeightFront;
        if (ScreenState == 4) {
            piLock(0);
            UpdateCar(lcd, RideHeightNormal, RideHeightDif);
            piUnlock(0);
        }
        

    }
}
