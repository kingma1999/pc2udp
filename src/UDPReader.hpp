#ifndef UDPREADER_HPP_
#define UDPREADER_HPP_

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include <algorithm>

#include "processing.hpp"
#include "SMS_UDP_Definitions.hpp"

class UDPReader
{
    private:
        char *mBuffer;
        int mLastBytesRead;

        const int mPort;
        sockaddr_in mReceiveAdress;
        int mSocketFD;

        void ReceivePacket();
        void HandleData();

        DataProcessor processor;
    protected:
    public:
        UDPReader(int port);
        ~UDPReader();

        void Listen();
};

#endif  // UDPREADER_HPP_
