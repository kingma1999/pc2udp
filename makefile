CC=g++

SRC_DIR=./src
BIN_DIR=./bin
OBJ_DIR=./obj

SRC=$(wildcard $(SRC_DIR)/*.cpp)
HDR=$(wildcard $(SRC_DIR)/*.hpp) $(wildcard $(SRC_DIR)/*.h)
OBJ=$(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SRC))

PRECOMPILES=UDPReader ftfs lcd main
PRECOMPILES_SRCS=$(patsubst %,$(SRC_DIR)/%.cpp,$(PRECOMPILES))
PRECOMPILES_OBJS=$(patsubst %,$(OBJ_DIR)/%.o,$(PRECOMPILES))
PRECOMPILES_TRGT=$(OBJ_DIR)/pres.a

SRC:=$(filter-out $(PRECOMPILES_SRCS),$(SRC))
OBJ:=$(filter-out $(PRECOMPILES_OBJS),$(OBJ))

DEBUG_EXE=$(BIN_DIR)/debug
RELEASE_EXE=$(BIN_DIR)/PC2Listener

debug: CFLAGS=-g -O0 -Wall -std=c++11
debug: LDFLAGS=-lwiringPi

release: CFLAGS=-Wall -std=c++11
release: LDFLAGS=-lwiringPi

.PHONY: release debug clean

release: $(RELEASE_EXE)

debug: $(DEBUG_EXE)

$(PRECOMPILES_TRGT): $(PRECOMPILES_OBJS)
	ar -rsc $@ $^

$(PRECOMPILES_OBJS): $(PRECOMPILES_SRCS) $(HDR)
	$(CC) $(CFLAGS) -c -o $@ $(patsubst obj/%.o,$(SRC_DIR)/%.cpp,$@)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(HDR)
	$(CC) $(CFLAGS) -c -o $@ $<

$(DEBUG_EXE): $(OBJ) $(PRECOMPILES_TRGT)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(RELEASE_EXE): $(OBJ) $(PRECOMPILES_TRGT)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	rm -rf $(OBJ) $(DEBUG_EXE) $(RELEASE_EXE)
