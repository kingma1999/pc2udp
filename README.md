# Project cars 2 UDP packet listener & processer

An UDP client that reads and interprets the packets from Project Cars 2's UDP stream containing telemetry and other statistics.
Everything is written in c++ and can be compiled on linux systems using the included makefile and g++.

## How it works
First the packets are read using a linux UDP socket. Then the data in the packets is converted to the relevant struct in the [src/UDP\_Definitions.hpp](https://gitlab.com/kingma1999/pc2udp/blob/master/src/UDP_Definitions.hpp).
These structs filled with the data are then passed to the functions declared in [src/processing.hpp](https://gitlab.com/kingma1999/pc2udp/blob/master/src/processing.hpp).
In these functions the data can be processed and shown to the user or passed on to another device.
Examples of these functions are available in [src/processing.cpp](https://gitlab.com/kingma1999/pc2udp/blob/master/src/processing.cpp).

## Features
Shows

## Why it was created
To make an Arduino controlled LCD display displaying different data the standard HUD of the game does not display.
